package mi

import "errors"

var ErrUserCantBeEmpty = errors.New("the user fields can't be empty")
var ErrUserNotFound = errors.New("user not found")

// User domain model.
type User struct {
	ID       int    `json:"id"`
	Username string `json:"username"`
}
