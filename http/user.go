package http

import (
	"errors"
	"net/http"
	"strconv"

	"gitlab.com/adrianolmedo/mi"
	"gitlab.com/adrianolmedo/mi/http/resp"

	"github.com/gofiber/fiber/v2"
)

func (h handlers) findUser(c *fiber.Ctx) error {
	id, err := strconv.Atoi(c.Params("id"))
	if id < 0 || err != nil {
		return c.Status(http.StatusBadRequest).JSON(resp.GetError(err))
	}

	user, err := h.srv.User.Find(id)
	if errors.Is(err, mi.ErrUserNotFound) {
		return c.Status(http.StatusNotFound).JSON(resp.GetError(err))
	}

	if err != nil {
		return c.Status(http.StatusBadRequest).JSON(resp.GetError(err))
	}

	return c.Status(http.StatusOK).JSON(resp.Data{
		Data: user,
	})
}
