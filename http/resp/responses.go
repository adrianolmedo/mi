package resp

type Ok struct {
	Msg string `json:"ok"`
}

type Error struct {
	Msg string `json:"error"`
}

func GetError(err error) Error {
	return Error{err.Error()}
}

type Data struct {
	Data any `json:"data"`
}

type OkData struct {
	Msg  string `json:"ok"`
	Data any    `json:"data"`
}

type ErrorData struct {
	Msg  string `json:"error"`
	Data any    `json:"data"`
}

type MetaData struct {
	Meta  any `json:"meta"`
	Data  any `json:"data"`
	Links any `json:"links,omitempty"`
}
