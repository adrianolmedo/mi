package http

import (
	"gitlab.com/adrianolmedo/mi/app"

	"github.com/gofiber/fiber/v2"
)

// Router set handlers and return the REST API framework.
func Router(s *app.Services) *fiber.App {
	h := &handlers{srv: s}
	f := fiber.New()
	f.Get("/v1/users/:id", h.findUser)
	return f
}

// handlers dependency manager for handlers.
type handlers struct {
	srv *app.Services
}
