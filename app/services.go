package app

import "gitlab.com/adrianolmedo/mi/pgsql"

// Services represents all appliaction logic layers.
type Services struct {
	User userService
}

func NewServices(db *pgsql.Queries) *Services {
	return &Services{
		User: userService{db},
	}
}
