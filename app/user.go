package app

import (
	"context"
	"database/sql"
	"errors"

	"gitlab.com/adrianolmedo/mi"
	"gitlab.com/adrianolmedo/mi/pgsql"
)

type userService struct {
	db *pgsql.Queries
}

func (u userService) Find(id int) (*mi.User, error) {
	if id == 0 {
		return nil, mi.ErrUserNotFound
	}

	user, err := u.db.GetUser(context.Background(), int64(id))
	if errors.Is(err, sql.ErrNoRows) {
		return nil, mi.ErrUserNotFound
	}

	// TODO: Capturar éste error con un logger y enviar un mensaje de error
	// en su lugar.
	if err != nil {
		return nil, err
	}

	return &mi.User{
		ID:       int(user.ID),
		Username: user.Username,
	}, nil
}
