package mi

// Config server.
type Config struct {
	// Port for address server, if it is empty by default it will be 80.
	Port string

	// HostDB when is running the database Engine.
	HostDB string

	// PortDB of database Engine server.
	PortDB string

	// UserDB of database, eg.: "root".
	UserDB string

	// PassDB is the password of User database.
	PassDB string

	// NameDB of SQL database.
	NameDB string
}
