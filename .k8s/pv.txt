apiVersion: v1
kind: PersistentVolume
metadata:
   name: shared-files-pv
   namespace: prueba
spec:
   capacity:
      storage: 1Gi
   accessModes:
      - ReadWriteMany
   hostPath:
      path: /home/adrian/php/mi
